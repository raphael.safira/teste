#baixa os BDOs dos dias anteriores
from calendar import formatstring
import pandas as pd
import datetime
import os
import urllib.request

pastaBDOs=os.getcwd()+'/BDOs'

#today='01-10-2021'
#day=pd.to_datetime(today,format='%d-%m-%Y').date()
today=datetime.datetime.today()
day=today

i=1
while (i<=7):
    day_excel=day.strftime('%d-%m-%Y')
    file="DIARIO_"+day_excel+".xlsx"
    day_download=day.strftime('%Y_%m_%d')
    outfilename = r'BDOs\\'+ file
    for root, dirs, files in os.walk(pastaBDOs, topdown=False): 
        if file in files: ## testa se o arquivo BDO ja foi baixado
            i=i+1
        else: ## se não foi ele tenta baixar
            try:
                url_of_file = 'http://sdro.ons.org.br/SDRO/DIARIO/'+day_download+'/Html/'+file
                urllib.request.urlretrieve(url_of_file, outfilename)
                print(outfilename)
                i=i+1
            except:## se o Boletim não tiver saido ainda
                print('Dia '+day_excel+' ainda não disponivel')
    MM= pd.to_timedelta(1,unit='d')
    day = day - MM